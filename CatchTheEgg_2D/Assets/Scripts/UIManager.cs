using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{
    //public
    public static UIManager instance;

    [Header("Settings: Game")]
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI timerText;

    [Header("Settings: Hud")]
    public Image StartGame;
    public Button Play;
    public GameObject Panel;
    public TextMeshProUGUI Win;
    public TextMeshProUGUI Lose;
    public Button Restart;

    //private
    private int score = 0;
    private bool StopGame;
    float myTime;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        myTime = Time.time;
        StartGame.gameObject.SetActive(true);

        if (StartGame == true)
        {
            Time.timeScale = 0;
            StopGame = true;
        }

        Play.onClick.AddListener(() => PlayGame());
        Restart.onClick.AddListener(() => RestartGame());
    }

    private void Update()
    {
        GameTime();
    }

    public void AddPoint()
    {
        score += 1;
        scoreText.text = "Score: " + score.ToString();

        if (score >= 15)
        {
            if (!StopGame)
            {
                Panel.SetActive(true);
                Win.gameObject.SetActive(true);
                Restart.gameObject.SetActive(true);
                Time.timeScale = 0;
                StopGame = true;
            }
        }
    }

    public void RemovePoint()
    {
        score -= 1;
        scoreText.text = "Score: " + score.ToString();

        if (score <= 0)
        {
            if (!StopGame)
            {
                Lose.gameObject.SetActive(true);
                Restart.gameObject.SetActive(true);
                Time.timeScale = 0;
                StopGame = true;
            }
        }
    }

    private void PlayGame()
    {
        StartGame.gameObject.SetActive(false);
        Time.timeScale = 1;
        StopGame = false;
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        StartGame.gameObject.SetActive(false);
        Time.timeScale = 1;
        StopGame = false;
    }

    private void GameTime()
    {
        float t = Time.time - myTime;

        float min = ((int)t / 59);
        float sec = (t % 59);

        if (sec < 10)
             timerText.text = "Time: " + min.ToString() + ":0" + sec.ToString("f0");
        else 
            timerText.text = "Time: " + min.ToString() + ":" + sec.ToString("f0");
    }  
}
