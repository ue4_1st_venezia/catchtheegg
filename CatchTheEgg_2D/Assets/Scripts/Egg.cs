using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Egg : MonoBehaviour
{
    //private
    private PlayerMovement Player;
    
    void Start()
    {
        Player = FindObjectOfType<PlayerMovement>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            EggPool.Instance.ReturnToPool(this);
            UIManager.instance.AddPoint();
        }

        if (collision.CompareTag("Border"))
        {
            EggPool.Instance.ReturnToPool(this);
            UIManager.instance.RemovePoint();
        }
    }
}
