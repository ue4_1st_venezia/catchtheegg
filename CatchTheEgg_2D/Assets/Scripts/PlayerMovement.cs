using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //public
    public float Speed;

    //private
    private Rigidbody2D Body;
    
    void Start()
    {
        Body = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        Movement();
    }

    void Movement()
    {
        Vector2 vel = Body.velocity;
        vel.x = Input.acceleration.x * Speed;
        Body.velocity = vel;
    }
}
