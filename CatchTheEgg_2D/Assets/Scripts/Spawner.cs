using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Header("Settings")]
    public GameObject Egg;
    public float MaxX;
    public float MinX;    
    public float TimeToSpawn;

    //private
    private float SpawnTime;
       
    void Update()
    {
        if(Time.time > SpawnTime)
        {
            float spawnX = Random.Range(MinX, MaxX);
            Vector3 spawnPos = new Vector3(spawnX, transform.position.y, transform.position.z);
            EggSpawn(spawnPos);
            SpawnTime = Time.time + TimeToSpawn;
        }
    }

    private void EggSpawn(Vector3 eggPos)
    {
        Egg egg = EggPool.Instance.Get();
        egg.transform.position = eggPos;
        egg.gameObject.SetActive(true);       
    }
}
