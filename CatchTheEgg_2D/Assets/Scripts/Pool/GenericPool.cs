using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenericPool<T> : MonoBehaviour where T : Component
{
    public static GenericPool<T> Instance { get; private set; }
    private Queue<T> pooledObj;
    [SerializeField] private T eggPrefabs;

    private void Awake()
    {
        Instance = this;
        pooledObj = new Queue<T>();
    }

    public T Get()
    {
        if(pooledObj.Count == 0)
        {
            AddEgg(5);
        }
        return pooledObj.Dequeue();
    }

    public void ReturnToPool(T eggReturn)
    {
        eggReturn.gameObject.SetActive(false);
        pooledObj.Enqueue(eggReturn);
    }

    public void AddEgg(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var obj = GameObject.Instantiate(eggPrefabs);
            obj.gameObject.SetActive(false);
            pooledObj.Enqueue(obj);
        }
    }
}
